'use strict';

/**
 * @class Represents MyWidget UU widget.
 */
function uuWidget() {
    var myWidget = new UUWidget({
        loginToken: "/Sites/Tasks/BaseWidget/my_widget/app/server/access", // Comment for Server!!!
        success: function (env) {
            var widget = new MyWidget(env);
        },
        error: function (env) {
            $('<div></div>').html('Widget "MyWidget" is not ready for use in view "default"!').appendTo('body');
        }
    });
};

var MyWidget = UUClass({
    id: 'widgetDashboard',
    filterBtn1Id: 'activeFilterBtn',
    filterBtn2Id: 'finalFilterBtn',
    columnCount: 0,

    /**
     * @constructor
     * @param {json} env
     * env.urlVariable.data={"sr":"UU:URI:TC.."}
     * env.urlVariable.options={'pageSize':'30','mode':'standard|debug','tabHeight':'500'}
     * set 'mode':'debug' enable conditional alert, console.log!
     */
    constructor: function (env) {
        var widget = this;

        //region Environment
        // Set Default Widget urlVariables
        env = env || {};
        env.urlVariables = env.urlVariables || {};
        env.urlVariables.data = env.urlVariables.data || {};
        env.urlVariables.data.sr = env.urlVariables.data.sr || {};
        env.urlVariables.options = env.urlVariables.options || {
                "mode": "debug"
            };
        env.urlVariables.options.mode = env.urlVariables.options.mode || 'standard';  //'debug'
        env.urlVariables.referreruri = env.urlVariables.referreruri || '';
        //endregion

        widget.loginToken = env.loginToken;
        widget.debug = (env.urlVariables.options.mode === 'debug');

        widget.loader = $('<div></div>').attr('id', 'loader').appendTo('body');
        widget.loader.jqxLoader({
            width: 100,
            height: 60,
            imagePosition: 'top',
            autoOpen: true,
            theme: 'uuTheme',
            text: widget.localizationConfig.loadingText
        });

        var dataRequest = $.ajax({
            type: 'GET',
            // hardcoded for now
            url: 'widget/contentForDefaultView/' + encodeURIComponent('40-118-1') + '/' + encodeURIComponent('Krásová Lídie'),
            cache: true,
            contentType: false,
            headers: {
                'TOKEN': widget.loginToken
            }
        });

        var containerTemplateRequest = $.ajax({
            url: 'templates/dockContainer.hbs',
            dataType: 'html'
        });
        var dockTemplateRequest = $.ajax({
            url: 'templates/singleDock.hbs',
            dataType: 'html'
        });

        widget.collapsedLayout = JSON.parse(localStorage.getItem(widget.id + 'CollapsedLayout'));
        widget.config = JSON.parse(localStorage.getItem(widget.id + 'config'));
        widget.layout = localStorage.getItem(widget.id + 'Layout');
        widget.layoutColumns = localStorage.getItem(widget.id + 'LayoutColumns');

        $.when(dataRequest, containerTemplateRequest, dockTemplateRequest)
            .done(function (dataResponse, containerResponse, dockResponse) {
                widget.createDashboard({
                    data: JSON.parse(dataResponse[0]).data.body.loaded,
                    container: containerResponse[0],
                    dock: dockResponse[0]
                });
            })
            .fail(function (error) {
                console.log('ERROR:');
                console.log(error);
            })
            .always(function () {
                widget.loader.jqxLoader("close");
            });
    },

    // dashboard init with main functionality
    createDashboard: function(viewModel) {
        var widget = this;

        var valueArray = widget.prepareDockingHtml(viewModel);

        // creating docks
        $('#' + widget.id).jqxDocking({
            mode: 'docked',
            theme: 'uuTheme',
            width: $(window).width()
        });

        // setting docks' dimensions and inserting charts
        for (var i = 0; i < valueArray.length; i++) {
            // note: jqxWidgets unfortunately don't return jQuery objects
            $('#' + widget.id).jqxDocking('setWindowProperty', valueArray[i].id, 'width', '425px');
            $('#' + widget.id).jqxDocking('setWindowProperty', valueArray[i].id, 'height', '255px');
            widget.insertChart('#' + valueArray[i].id + ' .resultsChart', valueArray[i].value);
        }

        // tabs for each dock
        $('#' + widget.id + ' .dockBody').jqxTabs({ width: '400px' });

        $('#' + widget.id).jqxDocking('hideAllCloseButtons');
        $('#' + widget.id).jqxDocking('showAllCollapseButtons');

        widget.loadDockingLayout();
        widget.initFilters();

        // charts animating on tab selection and dock expansion
        $('#' + widget.id + ' .dockBody').on('selected', animateChart);
        setTimeout(function() {
            $('#' + widget.id + ' .jqx-docking-window-uuTheme')
                .on('expand', function(event) {
                    animateChart(event);
                    widget.saveDockingLayout(event.target.id);
                })
                .on('collapse', function(event) {
                    widget.saveDockingLayout(event.target.id);
                });
        }, 500);

        function animateChart(event) {
            $(event.target).find('.resultsChart').jqxChart('refresh');
        }

        $('#' + widget.id).on('dragEnd', function(event) {
            widget.saveDockingLayout();
        });

    },

    initFilters: function() {
        var widget = this;

        var switchButtonSettings = {
            checked: true,
            onLabel: '<img src="assets/css/jquery-widgets/images/check_black.png"/>',
            offLabel: '<img src="assets/css/jquery-widgets/images/close_black.png"/>',
            width: '100px',
            height: '22px'
        };

        $('#' + widget.filterBtn1Id).jqxSwitchButton(switchButtonSettings);
        $('#' + widget.filterBtn2Id).jqxSwitchButton(switchButtonSettings);



        function toggleStateTypeFilter(isFinal, command) {
            if (isFinal) {
                widget.config.final = !widget.config.final;
            } else {
                widget.config.active = !widget.config.active;
            }
            localStorage.setItem(widget.id + 'config', JSON.stringify(widget.config));
            $('#' + widget.id).find('.jqx-window').each(function() {
                        // final sibjects have courseFinished header
                if (($(this).find('.courseFinished').length == isFinal) &&
                        // we don't want to collapse collapsed and expand expanded
                    ($(this).jqxWindow('collapsed') != (command == 'collapse'))) {
                    $(this).jqxWindow(command);
                }
            });

        }

        // init for first visit
        if (!widget.collapsedLayout) {
            widget.config = {
                active: true,
                final: true
            };
            toggleStateTypeFilter(true, 'collapse');
            $('#' + widget.filterBtn2Id).jqxSwitchButton('uncheck');
        } else {
            widget.config = widget.config || {
                    active: true,
                    final: true
            };
            if (!widget.config.active) {
                $('#' + widget.filterBtn1Id).jqxSwitchButton('uncheck');
            }
            if (!widget.config.final) {
                $('#' + widget.filterBtn2Id).jqxSwitchButton('uncheck');
            }
        }

        setTimeout(function(){
            $('#' + widget.filterBtn1Id)
                .on('unchecked', function(event) {
                    toggleStateTypeFilter(false, 'expand');
                })
                .on('checked', function(event) {
                    toggleStateTypeFilter(false, 'collapse');
                });

            $('#' + widget.filterBtn2Id)
                .on('unchecked', function(event) {
                    toggleStateTypeFilter(true, 'expand');
                })
                .on('checked', function(event) {
                    toggleStateTypeFilter(true, 'collapse');
                });

        }, 1000);
    },

    // inserting nice donut chart in given element
    insertChart: function (selector, points) {
        var widget = this;

        // drawing data
        var metrics = {
            value: (points > 120) ? 120 : points,
            // my choice
            max: 120
        };
        var data = [];
        data.push({ text: widget.localizationConfig.pointsGained, value: metrics.value }); // current
        data.push({ text: widget.localizationConfig.pointsRemaining, value: metrics.max - metrics.value }); // remaining

        var chartSettings = {
            description: '',
            enableAnimations: true,
            showLegend: false,
            source: data,
            showToolTips: true,
            animationDuration: 1500,
            seriesGroups:
                [
                    {
                        type: 'donut',
                        useGradientColors: false,
                        toolTipFormatSettings: {
                            decimalPlaces: 1,
                            decimalSeparator: ','
                        },
                        series:
                            [
                                {
                                    showLabels: false,
                                    enableSelection: true,
                                    displayText: 'text',
                                    dataField: 'value',
                                    labelRadius: 120,
                                    initialAngle: 90,
                                    radius: 65,
                                    innerRadius: 50,
                                    centerOffset: 0
                                }
                            ]
                    }
                ]
        };

        // number in the middle
        var valueText = points.toString().replace('.',',');
        chartSettings.drawBefore = function (renderer, rect) {
            var sz = renderer.measureText(valueText, 0, { 'class': 'chart-inner-text' });
            renderer.text(
                valueText,
                // we changed font from default Verdana to clearSans, so we have to adjust text position
                rect.x - 2 + (rect.width - sz.width) / 2,
                rect.y - 5 + rect.height / 2,
                0,
                0,
                0,
                { 'class': 'chart-inner-text' }
            );
        };

        // chart itsef
        $(selector).jqxChart(chartSettings);

        // green and red - gained and remaining chart parts
        $(selector).jqxChart('addColorScheme', 'customColorScheme', ['#6CBD49', '#FF3732']);
        $(selector).jqxChart({ colorScheme: 'customColorScheme' });

        // making the number in the middle colored according to points left to passing the subject
        $(selector).on('refreshEnd', function() {
            paintNumber(selector + ' .chart-inner-text', metrics.value, metrics.max);
        });
        paintNumber(selector + ' .chart-inner-text', metrics.value, metrics.max);

        function paintNumber(selector, points, maxPoints) {
            var red = [255,55,50];

            // brighter green than in uuTheme for brighter numbers when student "passed"
            var green = [46,233,58];
            //var green = [108,189,73];

            var multiplier = parseFloat(points/maxPoints);

            var computed = [];
            computed.push(Math.floor(red[0] * (1 - multiplier) + green[0] * (multiplier)));
            computed.push(Math.floor(red[1] * (1 - multiplier) + green[1] * (multiplier)));
            computed.push(Math.floor(red[2] * (1 - multiplier) + green[2] * (multiplier)));

            var finalStyle = 'rgb(' + computed + ')';

            // we're painting svg!
            $(selector).css('fill', finalStyle);
        }
    },

    localizationConfig: {
        // functionality
        loadingText: 'Načítání...',
        pointsGained: 'Získáno',
        pointsRemaining: 'Zbývá',
        activeLabel: 'Aktivní',
        finalLabel: 'Absolvované',

        // templates
        resultsTabLabel: 'Výsledky',
        gotoTabLabel: 'Přejít na',
        homeworks: 'Domácí úkoly',
        tests: 'Testy',
        seminarWork: 'Seminární práce',
        workshop: 'Workshop',
        total: 'Celkem',
        courseRun: 'Běh předmětu',
        courseRunStudentCard: 'Karta studenta v předmětu',

        // both toLocaleString and Intl.NumberFormat don't have wide enough browser support yet
        myLocaleNumberFormat: function(num) {
            return num.toFixed(1).replace('.',',');
        }
    },

    // adds HTML for jqxDocking and returns object with list of IDs of docks
    prepareDockingHtml: function (viewModel) {
        var widget = this;

        // templates init
        var dockTemplate = Handlebars.compile(viewModel.dock);
        var containerTemplate = Handlebars.compile(viewModel.container);

        // preparation for the hack below - jqxDocking orientation attribute doesn't really work
        // so we calculate how many columns would look nice
        var columnCount = Math.floor($(window).width() / 450);
        var rowCount = Math.floor(viewModel.data.length / columnCount);
        var lastRowLength = viewModel.data.length % columnCount;
        var lastRowRemaining = lastRowLength;

        // we will insert all docks into one string, which we will insert into main docking container
        var docks = "";

        // returned object
        var valueArray = [];

        for (var i = 0; i < viewModel.data.length; i++) {
            var course = viewModel.data[i];

            course.courseCode = course.courseCode.replace('.','').replace('-','');

            valueArray.push({
                // ids for later manipulation
                id: course.courseCode,
                // for charts drawing
                value: course.total.toFixed(1)
            });

            // uuUris for artifact links
            var courseRun = course.courseRun;
            var studentCardUes = course.courseRun.replace(/(ues\:\[[0-9]+\]\:)\[[0-9]+\]/, '$1') + '[' + course.studentCard + ']';

            // template itself
            var courseFinal = (course.stateType === 'FINAL')
            docks += dockTemplate({
                courseRunUes: encodeURIComponent(courseRun),
                studentCardUes: encodeURIComponent(studentCardUes),

                title: course.courseName,
                id: course.courseCode,
                dockStyle: (courseFinal) ? 'courseFinished' : 'courseActive',
                courseIco: (courseFinal) ? 'courseRunFinal.png' : 'courseRunActive.png',
                cardIco: (courseFinal) ? 'studentCardFinal.png' : 'studentCardActive.png',

                // points for table
                homeworkPts: widget.localizationConfig.myLocaleNumberFormat(course.learningActivities[0].score),
                testPts: widget.localizationConfig.myLocaleNumberFormat(course.learningActivities[1].score),
                seminarworkPts: widget.localizationConfig.myLocaleNumberFormat(course.learningActivities[2].score),
                workshopPts: widget.localizationConfig.myLocaleNumberFormat(course.learningActivities[3].score),
                totalPts: widget.localizationConfig.myLocaleNumberFormat(course.total),

                // template localization
                resultsTabLabel: widget.localizationConfig.resultsTabLabel,
                gotoTabLabel: widget.localizationConfig.gotoTabLabel,
                homeworks: widget.localizationConfig.homeworks,
                tests: widget.localizationConfig.tests,
                seminarWork: widget.localizationConfig.seminarWork,
                workshop: widget.localizationConfig.workshop,
                total: widget.localizationConfig.total,
                courseRun: widget.localizationConfig.courseRun,
                courseRunStudentCard: widget.localizationConfig.courseRunStudentCard
            });

            // jqxDocking doesn't support setting dock layout AND adds inline styles to docks and their containers
            // - so we have to hack around it to spread the docks nicely over the container
            if (lastRowRemaining > 0) {
                if (((i + 1) % (rowCount + 1)) === 0) {
                    // ending column and starting another
                    docks += '</div><div>';
                    lastRowRemaining--;
                }
            } else if (((i + 1) % rowCount) === (lastRowLength % rowCount)) {
                // ending column and starting another
                if ((i + 1) !== viewModel.data.length) {
                    docks += '</div><div>';
                }
            }

            widget.columnCount = columnCount;
        }

        // container for docking with action buttons
        $('body').append(containerTemplate({
            filter1BtnId: widget.filterBtn1Id,
            filter2BtnId: widget.filterBtn2Id,
            filter1BtnText: widget.localizationConfig.activeLabel,
            filter2BtnText: widget.localizationConfig.finalLabel,
            id: widget.id,
            docks: docks
        }));

        return valueArray;
    },

    // native jqxDocking Layout export function can't handle collapsed docks (library bug), so we have to handle those ourselves
    // when dock is collapsed/expanded method is called with the dock's id
    // when docks are rearranged method corrects jqxDocking layout settings so it can handle docks' position on next load
    saveDockingLayout: function(dockId) {
        var widget = this;

        if (dockId) {
            var idArray = JSON.parse(localStorage.getItem(widget.id + 'CollapsedLayout')) || [];
            var index = idArray.indexOf(dockId);
            if (index > -1) {
                idArray.splice(index, 1);
            } else {
                idArray.push(dockId);
            }
            localStorage.setItem(widget.id + 'CollapsedLayout', JSON.stringify(idArray));
        } else {
            // native jqxDocking layout settings
            var widgetLayout = JSON.parse($('#' + widget.id).jqxDocking('exportLayout'));

            // falsifying collapsed values
            for (var layoutSetting in widgetLayout) {
                // to filter out non-dock settings
                if (widgetLayout.hasOwnProperty(layoutSetting) && (layoutSetting.indexOf('panel') > -1))
                    for (var dock in layoutSetting) {
                        if (layoutSetting.hasOwnProperty(dock) && dock.collapsed) {
                            dock.collapsed = false;
                        }
                    }
            }

            localStorage.setItem(widget.id + 'Layout', JSON.stringify(widgetLayout));
            localStorage.setItem(widget.id + 'LayoutColumns', widget.columnCount);
        }
    },

    // loading our modified docking layout settings (see saveDockingLayout)
    loadDockingLayout: function() {
        var widget = this;

        if (widget.layout && (!widget.layoutColumns || (widget.layoutColumns == widget.columnCount))) {
            $('#' + widget.id).jqxDocking('importLayout', widget.layout);
        }

        // collapsing docks

        if (widget.collapsedLayout) {
            for (var i = 0; i < widget.collapsedLayout.length; i++) {
                $('#' + widget.id).jqxDocking('collapseWindow', widget.collapsedLayout[i]);
            }
        }
    }

});

// urgent

// required
// TODO presentation cliffnotes                                         0.5
// TODO copy basic theme improvements from widgetTable                  0.5
// TODO jqxNotifications / error function                               0.5
// TODO showing req in resultsTab                                       0.5
// TODO remember column count and load layout based on that             0.5

// polishing code
// TODO refactoring forceWidth                                          1
// TODO refactor column count hack                                      1
// TODO refactor timeout hack for collapsing windows onload             1
// TODO refactor styling headers and buttons                            1
// TODO refactor/comment stateType filter                               0.25

// wishlist
// TODO pretty icons for filterBtns                                     0.5
// TODO restyle loader                                                  2
// TODO server chewing data for client                                  3