# encoding: UTF-8

require 'uu_os_client'
require 'uu_objectstore'

class StudentCard
  SCORE_SCHEMA_CODE = "UCL.EDUCUA/SCORE"
  ASSESSMENT_DEFINITION_SCHEMA_CODE = "UCL.EDUCUA/ASSESSMENT_DEFINITION"

  OBJECT_STORE_URI = "ues:DEV0111-BT[84723967990072694]:R01-S58[34339947342785613]"

  # Kdyz chci pracovat s ObjectStorem z localhostu, tak musim vlozit toto:
  #
  # uu.objectstore.server-path-prefix=c00
  #
  # do
  #
  # ~/.uu/config/uu-client.properties

  def loadDataForStudent studentId, studentName
    subjects = []

    student_cards = loadStudentCards studentName

    student_card_codes = {}

    student_cards.each do |card|
      code = card.code
      subject = card.name.split(' - ')[2]

      student_card_codes[subject] = code
    end

    score_schema_uri = UU::OS::UESURIBuilder.parse_uesuri(OBJECT_STORE_URI).set_object_code(SCORE_SCHEMA_CODE).to_uesuri
    assessment_definition_schema_uri = UU::OS::UESURIBuilder.parse_uesuri(OBJECT_STORE_URI).set_object_code(ASSESSMENT_DEFINITION_SCHEMA_CODE).to_uesuri
    params = {:concurrency_strategy => UU::ObjectStore::ObjectStore::ConcurrencyStrategy::NONE}

    begin
      store = UU::ObjectStore::ObjectStore.init(OBJECT_STORE_URI, params)

      # test id 40-112-1
      data = store.query(score_schema_uri, :query => "data.studentUuid = '#{studentId}'")

      data.each do |item|
        subject = item.data
        student_card_codes.each do |subjectName, code|
          if subjectName.include? subject['courseName']
            subject['studentCard'] = code
            break
          end
        end
        subjects << subject
      end

        #data = store.load(:schema_uri => score_schema_uri, :data_key => 'ues:[84723967990090703]:[34339947381958992]')
        #data = store.load(:schema_uri => assessment_definition_schema_uri, :data_key => 'BIO-3.2')
        #store.save(student_score)
    ensure
      store.close
    end

    {loaded: subjects}
  end

  def loadStudentCards studentName
    card_list = UU::OS::Folder.get_entry_list 'ues:[84723967990090703]:FEDH201512-12.DATA/STUDENT_CARD_COURSES'
    card_list.reject { |studentCard| not studentCard.name.include?(studentName)}
  end
end