require_relative 'widget_controller'

class WidgetRootController < WidgetController
  get '/demo' do
    send_file File.join(settings.public_folder, 'demo.html')
  end

  get '/getContent' do
    send_file File.join(settings.public_folder, 'html/default.html')
  end
end
