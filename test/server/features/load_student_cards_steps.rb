require_relative '../../../app/server/model/student_card'

When(/^calling function loadStudentCards$/) do
  ::UU::OS::Security::Session.login('C:\Work\Repos\DashboardWidget\app\server\access')
  ::UU::OS::Security::Session.login('C:\Work\Repos\DashboardWidget\app\server\uuEE_access')

  @model = StudentCard.new
  @results = @model.loadStudentCards @student_name

  ::UU::OS::Security::Session.logout()
end

Then(/^we get array of cards for our student$/) do
  expect(@results.kind_of? Array).to eq(true)
  expect(@results.size).to be > 0
end

Then(/^not any other cards$/) do
  @results.each do |card|
    expect(card.name.split(' - ')[1]).to eq(@student_name)
  end
end
