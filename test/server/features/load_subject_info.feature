Feature: Load subject info
 Scenario: Load dashboard information
   Given student ID "40-118-1"
    And student name "Krásová Lídie"
    And subject "Programování"
   When loading data for student
   Then student in subject card UU artifact ID will be equal to "34339947381961869"
    And course run UES URL will be equal to "ues:[84723967990090703]:[34339947381960564]"