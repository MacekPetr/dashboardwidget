Feature: Load student cards for particular student
 Scenario: Standard functionality
   Given student name "Krásová Lídie"
   When calling function loadStudentCards
   Then we get array of cards for our student
   But not any other cards