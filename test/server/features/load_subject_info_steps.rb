require_relative '../../../app/server/model/student_card'

Given(/^student ID "([^"]*)"$/) do |student_id|
  @student_id = student_id
end

Given(/^student name "([^"]*)"$/) do |student_name|
  @student_name = student_name
end

Given(/^subject "([^"]*)"$/) do |subject|
  @subject = subject
end

When(/^loading data for student$/) do
  ::UU::OS::Security::Session.login('C:\Work\Repos\DashboardWidget\app\server\access')
  ::UU::OS::Security::Session.login('C:\Work\Repos\DashboardWidget\app\server\uuEE_access')

  @model = StudentCard.new
  @results = @model.loadDataForStudent @student_id, @student_name

  ::UU::OS::Security::Session.logout()
end

Then(/^student in subject card UU artifact ID will be equal to "([^"]*)"$/) do |card_uu_id|
  @results[:loaded].each do |course_object|
    if course_object[:courseName].equal? @subject
      expect(course_object[:studentCard]).to eq(card_uu_id)
      break
    end
  end

end

Then(/^course run UES URL will be equal to "([^"]*)"$/) do |course_url|
  @results[:loaded].each do |course_object|
    if course_object[:courseName].equal? @subject
      expect(course_object[:courseRun]).to eq(course_url)
      break
    end
  end
end
